# 7 expert

## Zusammenfassung

Healthchecks werden implementiert um den Container währent Laufzeit zu
überprüfen, da es auch Fehler gibt, bei denen der Prozess, der als
Entrypoint funktioniert nicht abstürzt. Dies können zum Beispiel
Probleme mit Abhänggkeiten (eg. Datenbank etc.) sein. Dadurch kann es
zu Exceptions kommen, bei denen der Prozess nicht abstürzt aber dem
Client ein 500er Error angezeigt wird. Dieser Healthcheck wird meisst
mit einem `curl` Befehl implementiert. Sollte dieser Fehlschlagen wird
etwas anderes als `0` als Bash Exit Code gesetzt und Docker erkennt dadurch,
dass etwas mit dem Container nicht stimmt.

## Implementierungen

### Dockerfile

In einem Dockerfile können Healthchecks mit der `HEALTHCHECK` Direktive
implementiert werden.
Beispiel bei einem Nginx Container:
```Dockerfile
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "curl", "http://localhost:80" ]
```

### Docker Compose file

Healthchecks können ebenfalls mit docker compose implementiert werden.
Beispiel:

```yaml
healthcheck:
  test: ["CMD", "curl", "http://localhost:80"]
  interval: 30s
  timeout: 10s
  retries: 3
```
